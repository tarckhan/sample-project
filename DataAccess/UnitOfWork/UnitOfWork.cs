﻿using DataAccess.Database;
using DataAccess.Repositories;
using Microsoft.EntityFrameworkCore.Storage;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private bool disposed = false;
        private readonly AppDbContext _dbContext;

        public IDbContextTransaction Transaction { get; set; }

        public UnitOfWork(AppDbContext dbContext) => _dbContext = dbContext;

        #region IUnitOfWork Members
        public IRepository<T> GetRepository<T>() where T : BaseEntity => new Repository<T>(_dbContext);

        public void BeginTransaction() => Transaction = _dbContext.Database.BeginTransaction();

        public void Commit()
        {
            try
            {
                Transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Rollback()
        {
            try
            {
                Transaction.Rollback();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveChanges()
        {
            try
            {
                return _dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region IDisposable Members
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }

            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
