﻿using DataAccess.Repositories;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity;

        int SaveChanges();

        void BeginTransaction();

        void Commit();

        void Rollback();
    }
}
