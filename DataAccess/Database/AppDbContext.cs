﻿using Microsoft.EntityFrameworkCore;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Database
{
    public partial class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }
        public virtual DbSet<AzercellContract> AzercellContracts { get; set; }
        public virtual DbSet<Branch> Branches { get; set; }
        public virtual DbSet<Citizenship> Citizenships { get; set; }
        public virtual DbSet<Claim> Claims { get; set; }
        public virtual DbSet<Contract> Contracts { get; set; }
        public virtual DbSet<ContractFile> ContractFiles { get; set; }
        public virtual DbSet<ContractFileType> ContractFileTypes { get; set; }
        public virtual DbSet<ContractStatus> ContractStatuses { get; set; }
        public virtual DbSet<DocumentInformation> DocumentInformations { get; set; }
        public virtual DbSet<DocumentType> DocumentTypes { get; set; }
        public virtual DbSet<Gender> Genders { get; set; }
        public virtual DbSet<IssuedCertificate> IssuedCertificates { get; set; }
        public virtual DbSet<OperationType> OperationTypes { get; set; }
        public virtual DbSet<Organization> Organizations { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RoleGroup> RoleGroups { get; set; }
        public virtual DbSet<RolesClaim> RolesClaims { get; set; }
        public virtual DbSet<SubscriptionType> SubscriptionTypes { get; set; }
        public virtual DbSet<Tarif> Tarifs { get; set; }
        public virtual DbSet<Token> Tokens { get; set; }
        public virtual DbSet<TokenStatus> TokenStatuses { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserFile> UserFiles { get; set; }
        public virtual DbSet<UserFileType> UserFileTypes { get; set; }
        public virtual DbSet<UserStatus> UserStatuses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<AzercellContract>(entity =>
            {
                entity.Property(e => e.Msisdn).IsRequired();

                entity.Property(e => e.Status).IsRequired();
            });

            modelBuilder.Entity<Branch>(entity =>
            {
                entity.HasIndex(e => e.OrganizationId, "IX_OrganizationId");

                entity.HasIndex(e => e.RegionId, "IX_RegionId");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNumber).IsRequired();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PlaceAddress)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.PlaceName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Organization)
                    .WithMany(p => p.Branches)
                    .HasForeignKey(d => d.OrganizationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Branches)
                    .HasForeignKey(d => d.RegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Citizenship>(entity =>
            {

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Claim>(entity =>
            {

            });

            modelBuilder.Entity<Contract>(entity =>
            {
                entity.HasIndex(e => e.BranchId, "IX_BranchId");

                entity.HasIndex(e => e.ContractStatusId, "IX_ContractStatusId");

                entity.HasIndex(e => e.DocumentInformationId, "IX_DocumentInformationId");

                entity.HasIndex(e => e.OperationTypeId, "IX_OperationTypeId");

                entity.HasIndex(e => e.TarifId, "IX_TarifId");

                entity.HasIndex(e => e.UserId, "IX_UserId");


                entity.Property(e => e.ContactNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DeliveryAddress).HasMaxLength(150);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Index)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.Contracts)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ContractStatus)
                    .WithMany(p => p.Contracts)
                    .HasForeignKey(d => d.ContractStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.DocumentInformation)
                    .WithMany(p => p.Contracts)
                    .HasForeignKey(d => d.DocumentInformationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.OperationType)
                    .WithMany(p => p.Contracts)
                    .HasForeignKey(d => d.OperationTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Tarif)
                    .WithMany(p => p.Contracts)
                    .HasForeignKey(d => d.TarifId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Contracts)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<ContractFile>(entity =>
            {
                entity.HasIndex(e => e.ContractFileTypeId, "IX_ContractFileTypeId");

                entity.HasIndex(e => e.ContractId, "IX_ContractId");

                entity.Property(e => e.ContractFileName).HasMaxLength(250);

                entity.HasOne(d => d.ContractFileType)
                    .WithMany(p => p.ContractFiles)
                    .HasForeignKey(d => d.ContractFileTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.ContractFiles)
                    .HasForeignKey(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ContractFileType>(entity =>
            {


                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ContractStatus>(entity =>
            {


                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DocumentInformation>(entity =>
            {
                entity.HasIndex(e => e.CitizenshipId, "IX_CitizenshipId");

                entity.HasIndex(e => e.DocumentTypeId, "IX_DocumentTypeId");

                entity.HasIndex(e => e.GenderId, "IX_GenderId");

                entity.Property(e => e.BirthAddress)
                    .IsRequired()
                    .HasMaxLength(250);



                entity.Property(e => e.DocumentNumber)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.DocumentOrganization)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.DocumentPin)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.DocumentSeries)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.EventDate)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ExpireDate)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Photo)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.RegisterCity)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.RegisterHousing)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.RegisterStreet)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Citizenship)
                    .WithMany(p => p.DocumentInformations)
                    .HasForeignKey(d => d.CitizenshipId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.DocumentType)
                    .WithMany(p => p.DocumentInformations)
                    .HasForeignKey(d => d.DocumentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Gender)
                    .WithMany(p => p.DocumentInformations)
                    .HasForeignKey(d => d.GenderId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<DocumentType>(entity =>
            {


                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Gender>(entity =>
            {


                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<IssuedCertificate>(entity =>
            {
                entity.Property(e => e.FirstCertificate).IsRequired();



            });



            modelBuilder.Entity<OperationType>(entity =>
            {


                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Organization>(entity =>
            {


                entity.Property(e => e.Contact)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Region>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();



                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasIndex(e => e.RoleGroupId, "IX_RoleGroupId");


                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Level)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.RoleGroup)
                    .WithMany(p => p.Roles)
                    .HasForeignKey(d => d.RoleGroupId)
                    ;
            });

            modelBuilder.Entity<RoleGroup>(entity =>
            {
                entity.HasIndex(e => e.OrganizationId, "IX_OrganizationId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Organization)
                    .WithMany(p => p.RoleGroups)
                    .HasForeignKey(d => d.OrganizationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<RolesClaim>(entity =>
            {
                entity.HasKey(e => new { e.RoleId, e.ClaimId });

                entity.HasIndex(e => e.ClaimId, "IX_ClaimId");

                entity.HasIndex(e => e.RoleId, "IX_RoleId");

                entity.HasOne(d => d.Claim)
                    .WithMany(p => p.RolesClaims)
                    .HasForeignKey(d => d.ClaimId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RolesClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<SubscriptionType>(entity =>
            {

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Tarif>(entity =>
            {
                entity.HasIndex(e => e.OrganizationId1, "IX_Organization_Id");

                entity.HasIndex(e => e.SubscriptionTypeId, "IX_SubscriptionTypeId");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);


                entity.HasOne(d => d.OrganizationId1Navigation)
                    .WithMany(p => p.Tarifs)
                    .HasForeignKey(d => d.OrganizationId1);

                entity.HasOne(d => d.SubscriptionType)
                    .WithMany(p => p.Tarifs)
                    .HasForeignKey(d => d.SubscriptionTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Token>(entity =>
            {
                entity.HasIndex(e => e.TokenStatusId, "IX_TokenStatusId");

                entity.HasIndex(e => e.UserId, "IX_UserId");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.HasOne(d => d.TokenStatus)
                    .WithMany(p => p.Tokens)
                    .HasForeignKey(d => d.TokenStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Tokens)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<TokenStatus>(entity =>
            {
                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => e.BranchId, "IX_BranchId");

                entity.HasIndex(e => e.RoleId, "IX_RoleId");

                entity.HasIndex(e => e.UserStatusId, "IX_UserStatusId");

                entity.Property(e => e.Contact).IsRequired();

                entity.Property(e => e.DocumentNumber)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.DocumentPin)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Password).IsRequired();

                entity.Property(e => e.Patronymic)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Photo)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.Salt).IsRequired();

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.UserStatus)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.UserStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<UserFile>(entity =>
            {
                entity.HasIndex(e => e.UserFileTypeId, "IX_UserFileTypeId");

                entity.HasIndex(e => e.UserId, "IX_UserId");

                entity.Property(e => e.UserFileName).HasMaxLength(250);

                entity.HasOne(d => d.UserFileType)
                    .WithMany(p => p.UserFiles)
                    .HasForeignKey(d => d.UserFileTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserFiles)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<UserFileType>(entity =>
            {

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<UserStatus>(entity =>
            {

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}