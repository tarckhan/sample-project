﻿using Common;
using Common.Enums.ErrorEnums;
using DataAccess.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Services.ServiceParameters.ReportServiceParameters;
using Services.ServiceParameters.ReportServiceParameters.AzpulReportServices;
using Services.ServiceParameters.ReportServiceParameters.GenericReportServices;
using Services.Services.EmailServices;
using Services.Services.ReportingServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Web_API;

namespace SmartcellReportingServices.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        private readonly IReportService _reportService;
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        public ReportsController(IReportService reportService, IConfiguration configuration, IEmailService emailService)
        {
            _reportService = reportService;
            _configuration = configuration;
            _emailService = emailService;
        }

        [HttpPost]
        [Route("DownloadAzpulBOKTReport")]
        public async Task<object> AzpulBOKTReport(AzpulReportDownloadInput input)
        {
            ContainerResult<object> containerResult = new ContainerResult<object>();
            string contentType = _configuration["ExcelContentType"];

            var result = await _reportService.GenerateAzpulReportForDownload(input);

            if (!result.IsSuccess)
            {
                containerResult.ErrorList.Add(new Error
                {
                    ErrorCode = result.ErrorList[0].ErrorCode,
                    ErrorMessage = result.ErrorList[0].ErrorMessage,
                    StatusCode = result.ErrorList[0].StatusCode,
                    Value = result.ErrorList[0].Value
                });
                return result;
            }
            return File(result.Output.OutputMemoryStream, contentType, $"Azpul MMC Report - {(Convert.ToDateTime(input.StartDate)).ToString("dd/MM//yyyy")} -  {(Convert.ToDateTime(input.StartDate)).ToString("dd/MM//yyyy")}");
        }

        [HttpPost]
        [Route("SendEmailAzpulBOKTReport")]
        public async Task<ContainerResult<string>> AzpulBOKTReport(AzpulReportSendEmailInput input)
        {
            ContainerResult<string> result = new ContainerResult<string>();

            var GeneratorResult = await _reportService.GenerateAzpulReportForDownload(new AzpulReportDownloadInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate
            });

            if (!GeneratorResult.IsSuccess)
            {
                result.ErrorList.Add(new Error
                {
                    ErrorCode = GeneratorResult.ErrorList[0].ErrorCode,
                    ErrorMessage = GeneratorResult.ErrorList[0].ErrorMessage,
                    StatusCode = GeneratorResult.ErrorList[0].StatusCode
                });
                return result;
            }
            return await _emailService.SendEmailAzpulBOKTReport(GeneratorResult.Output.OutputMemoryStream);
        }

        [HttpPost]
        [Route("DownloadGenericReport")]
        public async Task<object> DownloadGenericReport(DownloadGenericReportInput input)
        {
            ContainerResult<object> containerResult = new ContainerResult<object>();
            string contentType = _configuration["ExcelContentType"];

            var result = await _reportService.GenerateGenericReportForDownload(input);
            if (!result.IsSuccess)
            {
                containerResult.ErrorList.Add(new Error
                {
                    ErrorCode = result.ErrorList[0].ErrorCode,
                    ErrorMessage = result.ErrorList[0].ErrorMessage,
                    StatusCode = result.ErrorList[0].StatusCode,
                    Value = result.ErrorList[0].Value
                });
                return result;
            }
            return File(result.Output.OutputMemoryStream, contentType, $"{result.Output.OrganizationName} Report - {(Convert.ToDateTime(input.StartDate)).ToString("dd/MM//yyyy")} - {(Convert.ToDateTime(input.StartDate)).ToString("dd/MM//yyyy")}");
        }

        [HttpPost]
        [Route("SendEmailGenericReport")]
        public async Task<object> SendEmailGenericReport(SendEmailGenericReportInput input)
        {
            ContainerResult<string> result = new ContainerResult<string>();

            var GeneratorResult = await _reportService.GenerateGenericReportForDownload(new DownloadGenericReportInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                OrganizationId = input.OrganizationId
            });

            if (!GeneratorResult.IsSuccess)
            {
                result.ErrorList.Add(new Error
                {
                    ErrorCode = GeneratorResult.ErrorList[0].ErrorCode,
                    ErrorMessage = GeneratorResult.ErrorList[0].ErrorMessage,
                    StatusCode = GeneratorResult.ErrorList[0].StatusCode
                });
                return result;
            }
            return await _emailService.SendEmailGenericReport(GeneratorResult.Output.OutputMemoryStream, GeneratorResult.Output.OrganizationName);
        }
    }
}
