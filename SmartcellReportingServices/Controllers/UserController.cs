﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Services.ServiceParameters.UserServiceParamaters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [HttpPost]
        [Route("LoginUser")]
        public async Task<ContainerResult<LoginUserOutput>> LoginUser (LoginUserInput input)
        {
            await Task.CompletedTask;
            // TO BE IMPLEMENTED
            return null;
        }
        [HttpPost]
        [Route("LogoutUser")]
        public async Task<ContainerResult<LogoutUserOutput>> LogoutUser (LogoutUserInput input)
        {
            await Task.CompletedTask;
            // TO BE IMPLEMENTED
            return null;
        }
    }
}
