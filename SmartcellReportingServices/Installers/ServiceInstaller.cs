﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services;
using Services.Services.EmailServices;
using Services.Services.ReportingServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_API.Installers
{
    public class ServiceInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IReportService, ReportService>();

            var EmailConfiguration = configuration.GetSection("CustomEmailConfiguration").Get<EmailConfigurations>();
            services.AddSingleton(EmailConfiguration);


            services.AddTransient<IEmailService, EmailService>();
        }
    }
}
