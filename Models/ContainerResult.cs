﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ContainerResult<TOutput> 
    {
        public TOutput Output { get; set; }
        public List<Error> ErrorList { get; set; } = new List<Error>();
        public bool IsSuccess => !ErrorList.Any();
    }
}
