﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class UserFile : BaseEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public byte UserFileTypeId { get; set; }
        public string UserFileName { get; set; }
        public byte[] UserRawData { get; set; }

        public virtual User User { get; set; }
        public virtual UserFileType UserFileType { get; set; }
    }
}
