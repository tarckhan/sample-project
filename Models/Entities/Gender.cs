﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class Gender : BaseEntity
    {
        public Gender()
        {
            DocumentInformations = new HashSet<DocumentInformation>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DocumentInformation> DocumentInformations { get; set; }
    }
}
