﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class SubscriptionType : BaseEntity
    {
        public SubscriptionType()
        {
            Tarifs = new HashSet<Tarif>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<Tarif> Tarifs { get; set; }
    }
}
