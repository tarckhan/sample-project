﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class UserFileType : BaseEntity
    {
        public UserFileType()
        {
            UserFiles = new HashSet<UserFile>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<UserFile> UserFiles { get; set; }
    }
}
