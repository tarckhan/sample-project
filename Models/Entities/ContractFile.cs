﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class ContractFile : BaseEntity
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public byte ContractFileTypeId { get; set; }
        public string ContractFileName { get; set; }
        public byte[] ContractRawData { get; set; }

        public virtual Contract Contract { get; set; }
        public virtual ContractFileType ContractFileType { get; set; }
    }
}
