﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class UserStatus : BaseEntity
    {
        public UserStatus()
        {
            Users = new HashSet<User>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
