﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class AzercellContract : BaseEntity
    {
        public int Id { get; set; }
        public string Msisdn { get; set; }
        public string SimCardNumber { get; set; }
        public string ContactPhone1 { get; set; }
        public string ContactPhone2 { get; set; }
        public string City { get; set; }
        public string DealerUser { get; set; }
        public string DealerCode { get; set; }
        public string DealerPhone { get; set; }
        public string DealerAddress { get; set; }
        public string DealerDocument { get; set; }
        public string DealerFirstName { get; set; }
        public string DealerLastName { get; set; }
        public string DealerPatronymic { get; set; }
        public string DevicyType { get; set; }
        public string PaymentPlan { get; set; }
        public string Zip { get; set; }
        public string Email { get; set; }
        public string Imei { get; set; }
        public string Tariff { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string DocumentPin { get; set; }
    }
}
