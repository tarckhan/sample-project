﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class IssuedCertificate : BaseEntity
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int OrganizationId { get; set; }
        public DateTime IssuedDate { get; set; }
        public string FirstCertificate { get; set; }
        public string SecondCertificate { get; set; }
        public string FirstCardUUID { get; set; }
        public string SecondCardUUID { get; set; }
        public string DocumentPin { get; set; }
        public string DocumentNumber { get; set; }
        public string DocType { get; set; }
        public string RequestId { get; set; }
        public string FirstCSR { get; set; }
        public string SecondCSR { get; set; }
        public string IpAddress { get; set; }
    }
}
