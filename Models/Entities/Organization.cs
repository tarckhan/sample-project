﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class Organization : BaseEntity
    {
        public Organization()
        {
            Branches = new HashSet<Branch>();
            IssuedCertificates = new HashSet<IssuedCertificate>();
            RoleGroups = new HashSet<RoleGroup>();
            Tarifs = new HashSet<Tarif>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Description { get; set; }
        public string Photo { get; set; }
        public bool IsActive { get; set; }
        public DateTime AddedDate { get; set; }

        public virtual ICollection<Branch> Branches { get; set; }
        public virtual ICollection<IssuedCertificate> IssuedCertificates { get; set; }
        public virtual ICollection<RoleGroup> RoleGroups { get; set; }
        public virtual ICollection<Tarif> Tarifs { get; set; }
    }
}
