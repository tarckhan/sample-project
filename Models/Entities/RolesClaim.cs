﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class RolesClaim : BaseEntity
    {
        public int RoleId { get; set; }
        public int ClaimId { get; set; }

        public virtual Claim Claim { get; set; }
        public virtual Role Role { get; set; }
    }
}
