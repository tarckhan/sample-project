﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class TokenStatus : BaseEntity
    {
        public TokenStatus()
        {
            Tokens = new HashSet<Token>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Token> Tokens { get; set; }
    }
}
