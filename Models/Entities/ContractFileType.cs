﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class ContractFileType : BaseEntity
    {
        public ContractFileType()
        {
            ContractFiles = new HashSet<ContractFile>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ContractFile> ContractFiles { get; set; }
    }
}
