﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class Claim : BaseEntity
    {
        public Claim()
        {
            RolesClaims = new HashSet<RolesClaim>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime AddedDate { get; set; }

        public virtual ICollection<RolesClaim> RolesClaims { get; set; }
    }
}
