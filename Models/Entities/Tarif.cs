﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class Tarif : BaseEntity
    {
        public Tarif()
        {
            Contracts = new HashSet<Contract>();
        }

        public byte Id { get; set; }
        public byte OrganizationId { get; set; }
        public byte SubscriptionTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? OrganizationId1 { get; set; }

        public virtual Organization OrganizationId1Navigation { get; set; }
        public virtual SubscriptionType SubscriptionType { get; set; }
        public virtual ICollection<Contract> Contracts { get; set; }
    }
}
