﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class OperationType : BaseEntity
    {
        public OperationType()
        {
            Contracts = new HashSet<Contract>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Contract> Contracts { get; set; }
    }
}
