﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class Role : BaseEntity
    {
        public Role()
        {
            RolesClaims = new HashSet<RolesClaim>();
            Users = new HashSet<User>();
        }

        public int Id { get; set; }
        public int RoleGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Level { get; set; }
        public DateTime AddedDate { get; set; }

        public virtual RoleGroup RoleGroup { get; set; }
        public virtual ICollection<RolesClaim> RolesClaims { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
