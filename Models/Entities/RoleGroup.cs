﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.Entities
{
    public partial class RoleGroup : BaseEntity
    {
        public RoleGroup()
        {
            Roles = new HashSet<Role>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int OrganizationId { get; set; }

        public virtual Organization Organization { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}
