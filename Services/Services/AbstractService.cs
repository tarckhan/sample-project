﻿using Common;
using Common.Enums.DatabaseEnums;
using Common.Enums.ErrorEnums;
using Common.Resources;
using DataAccess.UnitOfWork;
using log4net;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public abstract class AbstractService : IService
    {
        protected readonly ILog _logger;
        protected readonly IUnitOfWork _uow;
        protected ConnectionTypes _connectionType;
        public AbstractService(IUnitOfWork uow, ILog logger = null)
        {
            _uow = uow;
            _logger = logger ?? LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        }

        protected async Task<ContainerResult<TOutput>> ExecuteAsync<TOutput>
            (ConnectionTypes connectionType, Func<Task<ContainerResult<TOutput>>> callBack)
        {
            ContainerResult<TOutput> result = new ContainerResult<TOutput>();
            try
            {
                _logger.Info($"Executing process started from this class : {GetType()}");

                _connectionType = connectionType;

                Begin();

                result = await callBack();

                if (!result.IsSuccess)
                {
                    RollBack();

                    _logger.Error($"Error occured from this class : {GetType()} : {result.ErrorList[0].ErrorMessage}");

                    return result;
                }

                Commit();

                _logger.Info($"Executing process finished from this class : {GetType()}");
            }
            catch (Exception ex)
            {

                result.ErrorList.Add(new Error
                {
                    ErrorCode = ErrorCodes.INTERNAL_ERROR,
                    ErrorMessage = ResourceENG.UNHANDLED_EXCEPTION,
                    StatusCode = ErrorHttpStatus.INTERNAL
                });

                RollBack();

                _logger.Error($"Error occured from this class : {GetType()} : {ex}");
            }


            return result;

        }


        protected void Begin()
        {
            if (_connectionType == ConnectionTypes.TRANSACTION)
            {
                _uow.BeginTransaction();
            }
        }

        protected void Commit()
        {
            if (_connectionType == ConnectionTypes.TRANSACTION)
            {
                _uow.Commit();
            }
        }

        protected void RollBack()
        {
            if (_connectionType == ConnectionTypes.TRANSACTION)
            {
                try
                {
                    _uow.Rollback();
                }
                catch (Exception ex)
                {
                    _logger.Error($"Error occured rollback : {ex}");
                }
            }

        }

        #region Disposable Members
        public void Dispose()
        {
            _uow.Dispose();
        }
        #endregion
    }
}
