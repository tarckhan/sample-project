﻿using Common;
using Common.Enums.DatabaseEnums;
using Common.Enums.ErrorEnums;
using Common.Enums.Organizations;
using Common.Resources;
using DataAccess.UnitOfWork;
using MailKit.Net.Smtp;
using MimeKit;
using Models;
using Services.EmailOperations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services.EmailServices
{
    public class EmailService : AbstractService, IEmailService
    {
        private readonly EmailConfigurations _emailConfigurations;
        public EmailService(EmailConfigurations emailConfigurations, IUnitOfWork uow) : base(uow)
        {
            _emailConfigurations = emailConfigurations;
        }

        public async Task<ContainerResult<string>> SendEmailAzpulBOKTReport(MemoryStream memoryStream)
            => await ExecuteAsync(ConnectionTypes.NONE, async () =>
            {
                ContainerResult<string> emailSenderResult = new ContainerResult<string>();

                var emailMessage = CreateMailMessage(memoryStream);

                bool isEmailSentSuccessfully = TrySend(emailMessage);

                if (isEmailSentSuccessfully)
                {
                    emailSenderResult.Output = $"Hesabat e-poçtu uğurla göndərildi!";
                    return emailSenderResult;
                }
                else
                {
                    emailSenderResult.ErrorList.Add(new Error
                    {
                        ErrorCode = ErrorCodes.UNAUTHORIZED,
                        ErrorMessage = ResourceENG.UNAUTHORIZED,
                        Value = "Email servisi cavab vermir."
                    });
                    return emailSenderResult;
                }

            });

        public async Task<ContainerResult<string>> SendEmailGenericReport(MemoryStream InputmemoryStream, string organizationName)
            => await ExecuteAsync(ConnectionTypes.NONE, async () =>
            {
                ContainerResult<string> result = new ContainerResult<string>();

                #region Creating the email Message

                _logger.Info($"Creating the Email Mesage Body.");
                string filename = $"Report - {organizationName} - {DateTime.Now.ToString("dd/MM/yyyy")}";

                var emailMessage = new MimeMessage();
                emailMessage.From.Add(new MailboxAddress(_emailConfigurations.From));
                emailMessage.To.Add(new MailboxAddress("tarxan.badirov@rabita.az"));
                emailMessage.Subject = filename;

                var bodyBuilder = new BodyBuilder() { };
                if (InputmemoryStream != null)
                {
                    bodyBuilder.Attachments.Add($"REPORT -- {organizationName}", InputmemoryStream.ToArray(),
                        ContentType.Parse("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
                }
                emailMessage.Body = bodyBuilder.ToMessageBody();
                #endregion

                #region Sending the actual email with SMTP
                using (var client = new SmtpClient())
                {
                    _logger.Info($"The created Email message is being sent. Sender: {_emailConfigurations.From}, SmtpServer: {_emailConfigurations.SmtpServer}");
                    try
                    {
                        client.Connect(_emailConfigurations.SmtpServer, _emailConfigurations.Port, true);
                        client.AuthenticationMechanisms.Remove("XOAUTH2");

                        client.Authenticate(_emailConfigurations.Username, _emailConfigurations.Password);

                        client.Send(emailMessage);

                        _logger.Info($"The Email has been sent. Receiver: {emailMessage.To}");
                    }
                    catch (Exception ex)
                    {
                        _logger.Error($"An error occurred while sending the email: {ex.ToString()}");

                        result.ErrorList.Add(new Error
                        {
                            ErrorCode = ErrorCodes.UNAUTHORIZED,
                            StatusCode = ErrorHttpStatus.FORBIDDEN,
                            ErrorMessage = ResourceENG.UNAUTHORIZED,
                            Value = "Email provayderi əməliyyata icazə vermədi."
                        });
                        return result;
                    }
                    finally
                    {
                        client.Disconnect(true);
                        client.Dispose();
                    }
                }
                #endregion

                return new ContainerResult<string>()
                {
                    Output = $"{organizationName} üçün hesabat faylı uğurla göndərildi."
                };
            });




        public MimeMessage CreateMailMessage(MemoryStream memoryStream)
        {
            _logger.Info($"Creating the Email Mesage Body.");
            string filename = $"Report - {Organizations.AzpulBOKT} - {DateTime.Now.ToString("dd/MM/yyyy")}";

            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress(_emailConfigurations.From));
            emailMessage.To.Add(new MailboxAddress("tarxan.badirov@rabita.az"));
            emailMessage.Subject = filename;

            var bodyBuilder = new BodyBuilder() { };
            if (memoryStream != null)
            {
                bodyBuilder.Attachments.Add("REPORT -- AZPUL BOKT", memoryStream.ToArray(),
                    ContentType.Parse("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
            }

            emailMessage.Body = bodyBuilder.ToMessageBody();

            return emailMessage;
        }
        private bool TrySend(MimeMessage mailMessage)
        {
            using (var client = new SmtpClient())
            {
                _logger.Info($"The created Email message is being sent. Sender: {_emailConfigurations.From}, SmtpServer: {_emailConfigurations.SmtpServer}");
                try
                {
                    client.Connect(_emailConfigurations.SmtpServer, _emailConfigurations.Port, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    var t = client.AuthenticationMechanisms.ToList();
                    client.Authenticate(_emailConfigurations.Username, _emailConfigurations.Password);

                    client.Send(mailMessage);

                    _logger.Info($"The Email has been sent. Receiver: {mailMessage.To}");
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.Error($"An error occurred while sending the email: {ex.ToString()}");
                    return false;
                }
                finally
                {
                    client.Disconnect(true);
                    client.Dispose();
                }
            }
        }

        
    }
}
