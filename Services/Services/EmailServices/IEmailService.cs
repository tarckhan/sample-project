﻿using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services.EmailServices
{
    public interface IEmailService
    {
        Task<ContainerResult<string>> SendEmailAzpulBOKTReport(MemoryStream InputmemoryStream);
        Task<ContainerResult<string>> SendEmailGenericReport(MemoryStream InputmemoryStream, string OrganizationName);

    }
}
