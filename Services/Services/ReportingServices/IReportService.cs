﻿using Models;
using Services.ServiceParameters.ReportServiceParameters;
using Services.ServiceParameters.ReportServiceParameters.AzpulReportServices;
using Services.ServiceParameters.ReportServiceParameters.GenericReportServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services.ReportingServices
{
    public interface IReportService
    {
        Task<ContainerResult<AzpulReportDownloadOutput>> GenerateAzpulReportForDownload(AzpulReportDownloadInput input);
        Task<ContainerResult<DownloadGenericReportOutput>> GenerateGenericReportForDownload(DownloadGenericReportInput input);
    }
}
