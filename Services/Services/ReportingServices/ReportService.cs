﻿using Common;
using Common.Enums.DatabaseEnums;
using Common.Enums.ErrorEnums;
using Common.Resources;
using DataAccess.Database;
using DataAccess.UnitOfWork;
using Models;
using Models.Entities;
using OfficeOpenXml;
using Services.ServiceParameters.ReportServiceParameters;
using Services.ServiceParameters.ReportServiceParameters.AzpulReportServices;
using Services.ServiceParameters.ReportServiceParameters.GenericReportServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services.ReportingServices
{

    public class ReportService : AbstractService, IReportService
    {

        public ReportService(IUnitOfWork unitOfWork) : base(unitOfWork) { }

        public async Task<ContainerResult<AzpulReportDownloadOutput>> GenerateAzpulReportForDownload(AzpulReportDownloadInput input)
            => await ExecuteAsync(ConnectionTypes.CONNECTION, async () =>
            {
                ContainerResult<AzpulReportDownloadOutput> result = new ContainerResult<AzpulReportDownloadOutput>();

                DateTime startingDate = DateTime.Now;
                DateTime endingDate = DateTime.Now;

                bool startDateParseResult = DateTime.TryParse(input.StartDate, out startingDate);
                bool endDateParseResult = DateTime.TryParse(input.EndDate, out endingDate);


                if (!startDateParseResult || !endDateParseResult)
                {
                    result.ErrorList.Add(new Error
                    {
                        ErrorCode = ErrorCodes.INCORRECT_DATE_FORMAT,
                        ErrorMessage = ResourceENG.INCORRECT_DATE_FORMAT,
                        StatusCode = ErrorHttpStatus.VALIDATION
                    });
                    return result;
                }

                TimeSpan timeSpan = endingDate - startingDate;
                if (timeSpan.TotalSeconds <= 0)
                {
                    result.ErrorList.Add(new Error
                    {
                        ErrorCode = ErrorCodes.INPUT_IS_NOT_VALID,
                        ErrorMessage = ResourceENG.INVALID_INPUT,
                        StatusCode = ErrorHttpStatus.VALIDATION,
                        Value = "Başlanğıc tarixi Son tarixdən əvvəl olmalıdır."
                    });
                    return result;
                }

                var ExcelOperationsResult = new ReportingService(_uow).GenerateAndSaveExcelFileFromCerts(startingDate, endingDate);

                if (ExcelOperationsResult != null)
                {
                    result.Output = new  AzpulReportDownloadOutput
                    {
                        OutputMemoryStream = ExcelOperationsResult,
                        ResultText = "Ready for download"
                    };
                    return result;
                }
                else
                {
                    result.ErrorList.Add(new Error
                    {
                        ErrorCode = ErrorCodes.INTERNAL_ERROR,
                        ErrorMessage = ResourceENG.UNHANDLED_EXCEPTION,
                        StatusCode = ErrorHttpStatus.INTERNAL
                    });

                    return result;
                }
            });


        public async Task<ContainerResult<DownloadGenericReportOutput>> GenerateGenericReportForDownload(DownloadGenericReportInput input)
            => await ExecuteAsync(ConnectionTypes.CONNECTION, async () =>
            {
                ContainerResult<DownloadGenericReportOutput> result = new ContainerResult<DownloadGenericReportOutput>();
                
                DateTime startingDate = DateTime.Now;
                DateTime endingDate = DateTime.Now;

                bool startDateParseResult = DateTime.TryParse(input.StartDate, out startingDate);
                bool endDateParseResult = DateTime.TryParse(input.EndDate, out endingDate);


                if (!startDateParseResult || !endDateParseResult)
                {
                    result.ErrorList.Add(new Error
                    {
                        ErrorCode = ErrorCodes.INCORRECT_DATE_FORMAT,
                        ErrorMessage = ResourceENG.INCORRECT_DATE_FORMAT,
                        StatusCode = ErrorHttpStatus.VALIDATION
                    });
                    return result;
                }

                TimeSpan timeSpan = endingDate - startingDate;

                if(timeSpan.TotalSeconds <= 0)
                {
                    result.ErrorList.Add(new Error
                    {
                        ErrorCode = ErrorCodes.INPUT_IS_NOT_VALID,
                        ErrorMessage = ResourceENG.INVALID_INPUT,
                        StatusCode = ErrorHttpStatus.VALIDATION,
                        Value = "Başlanğıc tarixi Son tarixdən əvvəl olmalıdır."
                    });
                    return result;
                }


                var OrganizationExists = _uow.GetRepository<Organization>().Get(org => org.Id == input.OrganizationId);

                if(OrganizationExists == null)
                {
                    result.ErrorList.Add(new Error
                    {
                        ErrorCode = ErrorCodes.ORGANIZATION_DOES_NOT_EXIST,
                        ErrorMessage = ResourceENG.ORGANIZATION_DOES_NOT_EXIST,
                        StatusCode = ErrorHttpStatus.NOT_FOUND,
                        Value = "organizationId yanlışdır."
                    });
                    return result;
                }

                var ExcelOperationsResult = new ReportingService(_uow).GenerateAndSaveExcelFileFromCerts(startingDate, endingDate, input.OrganizationId);

                if (ExcelOperationsResult != null)
                {
                    result.Output = new DownloadGenericReportOutput
                    {
                        OutputMemoryStream = ExcelOperationsResult,
                        ResultText = "Ready for download",
                        OrganizationName = OrganizationExists.Name
                    };
                    return result;
                }
                else
                {
                    result.ErrorList.Add(new Error
                    {
                        ErrorCode = ErrorCodes.INTERNAL_ERROR,
                        ErrorMessage = ResourceENG.UNHANDLED_EXCEPTION,
                        StatusCode = ErrorHttpStatus.INTERNAL
                    });

                    return result;
                }

            });

       
    }

    public class ReportingService 
    {
        private readonly IUnitOfWork _unitOfwork;
        public ReportingService(IUnitOfWork unitOfWork)
        {
            _unitOfwork = unitOfWork;
        }

        public MemoryStream GenerateAndSaveExcelFileFromCerts(DateTime startdate, DateTime enddate, int OrganizationId = 6)
        {
            Dictionary<int, HesabatServiceModel> dictionaryOfCerts = DictionaryAccounter(startdate.ToString(), enddate.ToString(), OrganizationId);


            MemoryStream memoryStream = new MemoryStream();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (ExcelPackage excelPackage = new ExcelPackage(memoryStream))
            {
                ExcelWorksheet workSheet = excelPackage.Workbook.Worksheets.Add("Hesabat");


                excelPackage.Workbook.Properties.Title = $"Hesabat - {DateTime.Now.ToString("M")}.xlsx";
                excelPackage.Workbook.Properties.Author = "Tarckhan Badirov (MHM, SXM)";

                workSheet.Cells["A1"].Value = "AD";
                workSheet.Cells["B1"].Value = "SOYAD";
                workSheet.Cells["C1"].Value = "FİN";
                workSheet.Cells["D1"].Value = "VERİLMƏ TARİXİ";


                int iterator = 2;
                for (int i = 0; i < dictionaryOfCerts.Count; i++)
                {
                    workSheet.Cells[iterator, 1].Value = dictionaryOfCerts[i].Name;
                    workSheet.Cells[iterator, 2].Value = dictionaryOfCerts[i].Surname;
                    workSheet.Cells[iterator, 3].Value = dictionaryOfCerts[i].Pin;
                    workSheet.Cells[iterator, 4].Value = dictionaryOfCerts[i].Date.ToString();
                    iterator++;
                }

                excelPackage.Save();
            }
            memoryStream.Position = 0;

            byte[] contents = memoryStream.ToArray();

            File.WriteAllBytes($@"D:\ExcelFiles\Hesabat{DateTime.Now.ToString("M")}.xlsx", contents);
            File.WriteAllBytes($@"D:\ExcelFiles\Hesabat.csv", contents);

            return memoryStream;

            #region Old One
            //using (var workBook = new XLWorkbook())
            //{
            //    workBook.Author = "Tarckhan Badirov (MHM, SXM)";

            //    var worksheet = workBook.Worksheets.Add("Hesabat");

            //    worksheet.Columns().AdjustToContents();
            //    worksheet.Cell(1, 1).Value = "AD";
            //    worksheet.Cell(1, 2).Value = "SOYAD";
            //    worksheet.Cell(1, 3).Value = "FIN";
            //    worksheet.Cell(1, 4).Value = "TARIX";

            //    var stream = new MemoryStream();

            //    using(ExcelPackage excelPackage = new ExcelPackage(stream))
            //    {

            //    }


            //    int iterator = 2;
            //    for (int i = 0; i < dictionaryOfCerts.Count; i++)
            //    {
            //        worksheet.Cell(iterator, 1).Value = dictionaryOfCerts[i].Name;
            //        worksheet.Cell(iterator, 2).Value = dictionaryOfCerts[i].Surname;
            //        worksheet.Cell(iterator, 3).Value = dictionaryOfCerts[i].Pin;
            //        worksheet.Cell(iterator, 4).Value = dictionaryOfCerts[i].Date;
            //        iterator++;
            //    }

            //    using (MemoryStream streams = new MemoryStream())
            //    {
            //        workBook.SaveAs(stream);

            //        byte [] contents = stream.ToArray();

            //        File.WriteAllBytes(@"D:\ExcelFiles\hesabatExcel.xlsx", contents);
            //        File.WriteAllBytes(@"D:\ExcelFiles\hesabat.csv", contents);
            //    }                  

            //}
            #endregion

        }


        public Dictionary<int, HesabatServiceModel> DictionaryAccounter(string startDateInput, string endDateInput, int organizationId = 6, int timeSpanSecondsForSeparation = 10)
        {
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;

            bool startDateParseResult = DateTime.TryParse(startDateInput, out startDate);
            bool endDateParseResult = DateTime.TryParse(endDateInput, out endDate);


            var ListofAllIssuedCerts = _unitOfwork.GetRepository<IssuedCertificate>().GetAll(
                 certs => certs.IssuedDate >= startDate
                 &&
                 certs.IssuedDate <= endDate
                 &&
                 certs.OrganizationId == organizationId

                 ).OrderBy(x => x.IssuedDate)
                 .ToList();

            StringBuilder names = new StringBuilder();
            StringBuilder surnames = new StringBuilder();
            StringBuilder dates = new StringBuilder();
            StringBuilder pins = new StringBuilder();


            Dictionary<int, HesabatServiceModel> outPutDictionary = new Dictionary<int, HesabatServiceModel>();

            int j = 0;

            for (int i = 0; i < ListofAllIssuedCerts.Count; i++)
            {
                int count = ListofAllIssuedCerts.Count;
                if (i == (count - 1))
                {
                    break;
                }

                string certificate = ListofAllIssuedCerts[i].FirstCertificate;
                byte[] csrValues = Encoding.ASCII.GetBytes(certificate);
                X509Certificate2 CurrentCertObject = new X509Certificate2(csrValues);
                var FullCurrentSubjectNameArray = CurrentCertObject.SubjectName.Name.ToString().Split(',');

                byte[] NextCertCsrValues = Encoding.ASCII.GetBytes(ListofAllIssuedCerts[i + 1].FirstCertificate);
                X509Certificate2 NextCertObject = new X509Certificate2(NextCertCsrValues);
                string[] FullNextSubjectNameArray = NextCertObject.SubjectName.Name.ToString().Split(',');


                string CurrentNameOnTheList = string.Empty;
                string CurrentSurnameOnTheList = string.Empty;
                DateTime IssuedDateOfTheCurrentCert = DateTime.Now;

                string NextNameOnTheList = string.Empty;
                string NextSurnameOnTheList = string.Empty;
                DateTime IssueDateOfTheNextCert = DateTime.Now;

                if (FullCurrentSubjectNameArray.Length == 6)
                {
                    CurrentNameOnTheList = FullCurrentSubjectNameArray[1].Substring(3);
                    CurrentSurnameOnTheList = FullCurrentSubjectNameArray[2].Substring(4);
                    IssuedDateOfTheCurrentCert = ListofAllIssuedCerts[i].IssuedDate;
                }
                if (FullCurrentSubjectNameArray.Length == 5)
                {
                    CurrentNameOnTheList = FullCurrentSubjectNameArray[0].Substring(2);
                    CurrentSurnameOnTheList = FullCurrentSubjectNameArray[1].Substring(4);
                    IssuedDateOfTheCurrentCert = ListofAllIssuedCerts[i].IssuedDate;
                }

                if (FullNextSubjectNameArray.Length == 6)
                {
                    NextNameOnTheList = FullNextSubjectNameArray[1].Substring(3);
                    NextSurnameOnTheList = FullNextSubjectNameArray[2].Substring(4);
                    IssueDateOfTheNextCert = ListofAllIssuedCerts[i + 1].IssuedDate;
                }
                if (FullNextSubjectNameArray.Length == 5)
                {
                    NextNameOnTheList = FullNextSubjectNameArray[0].Substring(2);
                    NextSurnameOnTheList = FullNextSubjectNameArray[1].Substring(4);
                    IssueDateOfTheNextCert = ListofAllIssuedCerts[i + 1].IssuedDate;
                }

                TimeSpan DurationBetweenCurrentAndNextCerts = IssueDateOfTheNextCert - IssuedDateOfTheCurrentCert;

                if (CurrentNameOnTheList == NextNameOnTheList
                    && CurrentSurnameOnTheList == NextSurnameOnTheList
                    && DurationBetweenCurrentAndNextCerts.TotalSeconds < timeSpanSecondsForSeparation)
                {
                    continue;
                }
                else
                {
                    names.Append(CurrentNameOnTheList + "\n");
                    surnames.Append(CurrentSurnameOnTheList + "\n");
                    dates.Append(IssuedDateOfTheCurrentCert + "\n");
                    pins.Append(ListofAllIssuedCerts[i].DocumentPin + "\n");


                    outPutDictionary.Add(j, new HesabatServiceModel()
                    {
                        Name = CurrentNameOnTheList,
                        Surname = CurrentSurnameOnTheList,
                        Date = IssuedDateOfTheCurrentCert.ToString(),
                        Pin = ListofAllIssuedCerts[i].DocumentPin
                    });

                    j++;
                }
            }

            return outPutDictionary;
        }


        #region METHODS FOR RECOVERING CERTIFICATES (WHICH WERE ACCIDENTALLY DELETED FROM THE DATABASE) FROM LOG FILES

        public List<string> AllCertRequestsFromLogFiles()
        {
            List<string> certificates = new List<string>();


            for (int i = 1; i <= 9; i++)
            {
                string logFileUrl = @$"D:\CustomLogs\Log{i}.txt";

                if (!File.Exists(logFileUrl))
                {
                    return new List<string>();
                }

                string fullLogContent = string.Empty;

                using (StreamReader fileReader = new StreamReader(logFileUrl))
                {

                    fullLogContent = fileReader.ReadToEnd();

                    bool areLogsFinished = false;
                    while (!areLogsFinished)
                    {

                        int startIndex = 0;

                        if (fullLogContent.Contains("Executing process started from this class : FaceRecognizer.BusinessLogic.Logic.CertificateLogic.GetCertificate"))
                        {
                            startIndex = fullLogContent.IndexOf("Executing process started from this class : FaceRecognizer.BusinessLogic.Logic.CertificateLogic.GetCertificate");
                        }
                        else
                        {
                            areLogsFinished = true;
                        }


                        int finishIndex = fullLogContent
                            .IndexOf("Executing process finished from this class : FaceRecognizer.BusinessLogic.Logic.CertificateLogic.GetCertificate");
                        finishIndex = finishIndex + 111;

                        string TrimmedCertificateContent = fullLogContent.Substring(startIndex, Math.Abs(finishIndex - startIndex));


                        bool isCertificateSavedToDatabase = TrimmedCertificateContent.Contains("CERTIFICATE(S) HAVE BEEN SAVED TO THE DATABASE SUCCESSFULLY");

                        fullLogContent = fullLogContent.Substring(finishIndex);

                        if (isCertificateSavedToDatabase)
                        {
                            certificates.Add(TrimmedCertificateContent);
                        }

                    }

                }
            }

            return certificates;
        }
        public void ParseAndSaveCertificates()
        {
            List<string> Certificates = AllCertRequestsFromLogFiles();

            for (int i = 0; i < Certificates.Count; i++)
            {
                #region FIELDS
                int userId = 5000;
                int organizationId = 6;
                DateTime issuedDate = DateTime.Now;
                string firstCertificate = string.Empty;
                string secondCertificate = string.Empty;
                string firstCardUUID = string.Empty;
                string secondCardUUID = string.Empty;
                string documentPin = string.Empty;
                string documentNumber = string.Empty;
                string docType = string.Empty;
                int? requestId = null;
                string firstCSR = string.Empty;
                string secondCSR = null;
                string ipAddress = string.Empty;
                #endregion

                string singleCertificate = Certificates[i];

                int issuedDateStartIndex = singleCertificate.IndexOf("FaceRecognizer.BusinessLogic.Logic.CertificateLogic.GetCertificate");


                issuedDate = DateTime.Parse(singleCertificate.Substring(117, 19));



                int csrStartIndex = (singleCertificate.IndexOf("CSRs")) + 8;

                int csrFinishIndex = singleCertificate.IndexOf("\"],");

                string CSRsFromSingleCert = singleCertificate.Substring(csrStartIndex, (Math.Abs(csrFinishIndex - csrStartIndex)));


                if (CSRsFromSingleCert.IndexOf(',') == -1)
                {
                    continue;
                }


                try
                {
                    firstCSR = CSRsFromSingleCert.Substring(0, (CSRsFromSingleCert.IndexOf(',')) - 1);
                }
                catch (Exception ex)
                {
                    var x = i;
                }


                if (CSRsFromSingleCert.Contains(","))
                {
                    secondCSR = CSRsFromSingleCert.Substring((CSRsFromSingleCert.IndexOf(',')) + 2);
                }

                singleCertificate = singleCertificate.Substring(csrFinishIndex);


                int startindexOfDocumentNumber = singleCertificate.IndexOf("DocumentNumber") + 17;
                int finishindexOfDocumentNumber = singleCertificate.IndexOf("DocumentNumber") + 7;
                documentNumber = singleCertificate.Substring(startindexOfDocumentNumber, Math.Abs(finishindexOfDocumentNumber - startindexOfDocumentNumber));

                if (documentNumber.Contains("\","))
                {
                    documentNumber = documentNumber.Remove(7);

                }

                int startindexOfDocumentPin = singleCertificate.IndexOf("DocumentPin") + 14;
                int finishindexOfDocumentPin = singleCertificate.IndexOf("DocumentPin") + 7;
                documentPin = singleCertificate.Substring(startindexOfDocumentPin, Math.Abs(finishindexOfDocumentPin - startindexOfDocumentPin));


                int startindexOfRequestId = singleCertificate.IndexOf("RequestId") + 11;
                int finishindexOfRequestId = singleCertificate.IndexOf("RequestId") + 7;
                int t = 0;
                int.TryParse(singleCertificate.Substring(startindexOfRequestId, Math.Abs(finishindexOfRequestId - startindexOfRequestId)), out t);

                requestId = t == 0 ? null : t;

                singleCertificate = singleCertificate.Substring(singleCertificate.IndexOf("CertificateData"));


                int startindexOffirstCertificate = singleCertificate.IndexOf("\"Certificate\":\"") + 15;
                int finishindexOffirstCertificate = singleCertificate.IndexOf("\"UUID\":\"");
                firstCertificate = singleCertificate.Substring(startindexOffirstCertificate, Math.Abs(finishindexOffirstCertificate - startindexOffirstCertificate) - 2);
                try
                {
                    singleCertificate = singleCertificate.Substring(singleCertificate.IndexOf("UUID"));
                }
                catch (Exception ex)
                {
                    int ab = i;
                    string d = ex.Message;
                }


                int startindexOffirstUUID = singleCertificate.IndexOf("UUID") + 7;
                int finishindexOffirstUUID = singleCertificate.IndexOf("Certificate") - 5;
                firstCardUUID = singleCertificate.Substring(startindexOffirstUUID, Math.Abs(finishindexOffirstUUID - startindexOffirstUUID));

                try
                {
                    singleCertificate = singleCertificate.Substring(singleCertificate.IndexOf("Certificate"));
                }
                catch (Exception)
                {
                    secondCertificate = null;
                    secondCardUUID = null;
                }


                int startindexOfSecondCertificate = singleCertificate.IndexOf("\"Certificate\":\"") + 15;
                int finishindexOfSecondCertificate = singleCertificate.IndexOf("\",");
                try
                {
                    secondCertificate = singleCertificate.Substring(startindexOfSecondCertificate, Math.Abs(finishindexOfSecondCertificate - startindexOfSecondCertificate) - 2);

                }
                catch (Exception ex)
                {
                    int ab = i;
                    string a = ex.Message;
                }


                int startindexOfSecondUUID = singleCertificate.IndexOf("UUID") + 7;
                int finishindexOfSecondUUID = singleCertificate.IndexOf("Status") - 5;
                secondCardUUID = singleCertificate.Substring(startindexOfSecondUUID, Math.Abs(finishindexOfSecondUUID - startindexOfSecondUUID));

                //IssuedCertificate issuedCertificate = new IssuedCertificate()
                //{
                //    UserId = userId,
                //    OrganizationId = organizationId,
                //    IssuedDate = issuedDate,
                //    FirstCertificate = firstCertificate,
                //    SecondCertificate = secondCertificate,
                //    FirstCardUUID = firstCardUUID,
                //    SecondCardUUID = secondCardUUID,
                //    DocumentPin = documentPin,
                //    DocumentNumber = documentNumber,
                //    DocType = "ID",
                //    RequestId = requestId,
                //    FirstCSR = firstCSR,
                //    SecondCSR = secondCSR,
                //    IpAddress = null
                //};


                //_appDbContext.IssuedCertificates.Add(issuedCertificate);
                //_appDbContext.SaveChanges();


            }

        }

        #endregion
    }
}
