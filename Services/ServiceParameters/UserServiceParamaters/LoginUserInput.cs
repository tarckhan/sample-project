﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ServiceParameters.UserServiceParamaters
{
    public class LoginUserInput
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
