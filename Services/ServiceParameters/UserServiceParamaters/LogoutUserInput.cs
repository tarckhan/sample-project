﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ServiceParameters.UserServiceParamaters
{
    public class LogoutUserInput
    {
        public string Jti { get; set; }
    }
}
