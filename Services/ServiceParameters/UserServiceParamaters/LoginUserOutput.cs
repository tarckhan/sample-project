﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ServiceParameters.UserServiceParamaters
{
    public class LoginUserOutput
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public int UserId { get; set; }
        public string userRoleGroup { get; set; }
        public DateTime JwtTokenExpireDate { get; set; }
        public DateTime RefreshTokenExpireDate { get; set; }
    }
}
