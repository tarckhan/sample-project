﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ServiceParameters.ReportServiceParameters
{
    public class HesabatServiceModel
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Pin { get; set; }
        public string Date { get; set; }
    }
}
