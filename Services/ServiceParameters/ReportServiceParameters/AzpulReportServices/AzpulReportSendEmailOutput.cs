﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ServiceParameters.ReportServiceParameters.AzpulReportServices
{
    public class AzpulReportSendEmailOutput
    {
        public string ResultText { get; set; }
        public bool  Status { get; set; }
    }
}
