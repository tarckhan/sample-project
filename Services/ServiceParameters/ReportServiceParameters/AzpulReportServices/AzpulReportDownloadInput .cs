﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Services.ServiceParameters.ReportServiceParameters
{
    public class AzpulReportDownloadInput
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        [JsonIgnore]
        public int SeparationSeconds { get; set; } = 10;
    }
}
