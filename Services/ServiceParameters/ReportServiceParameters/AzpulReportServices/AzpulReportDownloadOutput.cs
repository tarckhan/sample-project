﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ServiceParameters.ReportServiceParameters
{
    public class AzpulReportDownloadOutput
    {
        public string ResultText { get; set; }
        public MemoryStream OutputMemoryStream { get; set; }
    }
}