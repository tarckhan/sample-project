﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ServiceParameters.ReportServiceParameters.GenericReportServices
{
    public class DownloadGenericReportOutput
    {
        public string ResultText { get; set; }
        public MemoryStream OutputMemoryStream { get; set; }
        public string OrganizationName { get; set; }
    }
}
