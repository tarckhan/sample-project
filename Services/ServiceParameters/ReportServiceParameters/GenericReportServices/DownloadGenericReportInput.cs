﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Services.ServiceParameters.ReportServiceParameters.GenericReportServices
{
    public class DownloadGenericReportInput
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int OrganizationId { get; set; }
        [JsonIgnore]
        public int SeparationSeconds { get; set; } = 10;
    }
}
