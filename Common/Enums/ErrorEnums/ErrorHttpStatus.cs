﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums.ErrorEnums
{
	public enum ErrorHttpStatus : int
	{
		SUCCESS = 200,
		VALIDATION = 400,
		UNAUTHORIZED = 401,
		FORBIDDEN = 403,
		NOT_FOUND = 404,
		INTERNAL = 500,
		FAILED_LOGOUT = 501
	}
}
