﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums.DatabaseEnums
{
    public enum ConnectionTypes : byte
    {
        NONE = 0,
        CONNECTION,
        TRANSACTION
    }
}
