﻿using Common.Enums.ErrorEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public sealed class Error
    {
        public ErrorHttpStatus StatusCode { get; set; }
        public ErrorCodes ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string Value { get; set; }
    }
}
